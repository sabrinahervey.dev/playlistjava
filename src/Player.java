public class Player {
    public static void main(String[] args) throws Exception {
       // Création d'une instance de la classe Artist avec le prénom "Ozzy" et le nom "Osbourne"
        Artist monArtiste = new Artist("Ozzy", "Osbourne");
        Artist monArtiste2 = new Artist("Alice", "Cooper");
        // Création d'une instance de la classe Music avec le titre "Paranoid", une durée de 240 secondes 
        // et le tableau d'artistes contenant l'instance de l'artiste créée précédemment
        Music paranoid = new Music("Paranoid", 240, monArtiste);
        Music poison = new Music("Poison", 240, monArtiste2);
        
        //création de la playlist 
        Playlist playlist = new Playlist();
        //ajout de la chanson dans la playlist avec la méthode .add()
        playlist.add(poison);
        playlist.add(paranoid);
        System.out.println("durée total playlist " + playlist.getTotalDuration());

        playlist.next();

        // Affichage du nom complet de l'artiste en utilisant la méthode getFullName() de la classe Artist
        System.out.println("Artiste " + monArtiste.getFullName());

        // Affichage des informations de la musique en utilisant la méthode getInfos() de la classe Music
        System.out.println("title " + poison.getInfos());
    }

}

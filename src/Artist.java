
public class Artist {
    private String firstName;
    private String lastName;

    public Artist(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    //méthode pour obtenir les infos sur l'artiste
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }
    
}

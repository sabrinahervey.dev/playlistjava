
// Définition de la classe Music
public class Music {
    // Déclaration des variables
    private String title;
    private int duration;
    private Artist[] artistSet;
    
    // Constructeur de la classe Music
    public Music(String title, int duration, Artist[] artistSet) {
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    // constructeur pour un seul Artist
    public Music(String title, int duration, Artist artiste) {
        this.title = title;
        this.duration = duration;
        Artist[] listeartiste = { artiste };
        this.artistSet = listeartiste;
    }
    
    public int getDuration() {
        //je retourne la durée du morceau
        return duration;
    }

    // méthode pour obtenir les info de la musique
    public String getInfos() {
        int minutes = duration / 60; 
        int seconds = duration % 60;

        // Créer une chaîne de caractères formatée pour la durée au format mm:ss
        String durationString = String.format("duree: %02d:%02d", minutes, seconds);
        
        // Retourne une chaîne de caractères contenant le titre de la musique,
        // la durée formatée et les noms des artistes
        return this.title + " " + durationString + " " + this.artistSet[0].getFullName();
        
    }
}

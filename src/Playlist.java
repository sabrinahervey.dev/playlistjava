import java.util.ArrayList;

public class Playlist {
    private Music currentMusic;
    private ArrayList<Music> musicList = new ArrayList<>();

    public Playlist() {
        this.currentMusic = null;
    }

    // Ajoute une musique à la liste
    public void add(Music music) {
        musicList.add(music);
    }

    // Supprime une musique de la liste
    public void remove(Music music) {
        musicList.remove(music);
    }

    // passer à la musique suivante
    public void next() {
        // Vérifier si la liste de musique n'est pas vide
        if (!this.musicList.isEmpty()) {
            // Définir la première musique de la liste comme currentMusic
            this.currentMusic = this.musicList.get(0);
            // Retirer la musique en cours de la liste
            this.remove(this.currentMusic);
            // Afficher un message pour indiquer la musique en cours de lecture 
            System.out.println("Lecture en cours " + currentMusic.getInfos());
        } else {
            System.out.println("Musique non trouvée ");
        }
    }
    // Calcule la durée totale de la playlist
    public int getTotalDuration() {
        int totalDuration = 0;
        for (Music music : musicList) {
            totalDuration += music.getDuration();
        }
        return totalDuration;
    }
}